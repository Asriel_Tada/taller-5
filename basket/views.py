from django.shortcuts import render , redirect, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm,SetPasswordForm, AuthenticationForm, UserChangeForm
from django.contrib.auth.models import User
from django.contrib import messages
from basket.models import Coach
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, CreateView


# Create your views here.
from django.shortcuts import render, redirect, get_object_or_404
from .forms import *
from basket.models import (
    Team,
    Player,
    Coach,
    Match
)
@login_required
def players_list(request):
    template_name = 'players_list.html'
    data = {}

    teams = Team.objects.all()

    for team in teams:
        team.players = Player.objects.filter(team=team)

    data['teams'] = teams

    return render(request, template_name, data)
@login_required
def teams_list(request):
    template_name = 'teams_list.html'
    data = {}

    data['teams'] = Team.objects.filter(coach =request.user.Coach)
    return render(request, template_name, data)
@login_required
def coachs_list(request):
    template_name = 'coach_list.html'
    data = {}
    data['coachs'] = Coach.objects.all()

    return render(request, template_name, data)

@login_required
def matchs_list(request):
    template_name = 'acciones/matchs_list.html'
    data = {}

    data['matchs'] = Match.objects.all()

    return render(request, template_name, data)

def home(request):
    template_name = 'home.html'
    data = {}
    
    if request.user.is_authenticated:
        userrr = get_object_or_404(User, username=request.user.username)
        try:
            if  request.user.is_staff:
                data['teams'] = Team.objects.all().order_by('-pk')[:3]
                data['last_5_matchs'] = Match.objects.all().order_by('-pk')[:3]
                data['last_5_players'] = Player.objects.all().order_by('-pk')[:3]
                return render(request, template_name, data)
            else:
                prop = Coach.objects.get(user=userrr)
                q=Team.objects.filter(coach= request.user.coach.id)
                print (q)
                data['teams'] = q
                a= Match.objects.all()
                z = []
                for keys in a:
                    for yap in q:
                        print (yap.code)
                        print (keys.team1.code)
                        print (keys.team2.code)
                        keys.team1
                        if (keys.team1==yap ):  
                            v=keys
                            z.append(v)
                        elif (keys.team2==yap ):  
                            v=keys
                            z.append(v)
                data['last_5_matchs'] = z

                print (z)
                b=Player.objects.all()
                y= []
                for ap in b:
                    for ya in q:
                        print (ya)
                        if (ap.team == ya): 
                            ad=ap 
                            y.append(ap)
                data['last_5_players'] = y   

                
#                data['last_5_matchs'] = Match.objects.filter(team1 or team2 ).order_by('-pk')[:5]
#                data['last_5_players'] = Player.objects.filter(team=q).order_by('-pk')[:5]
                data['teams'] = Team.objects.filter(coach= request.user.coach.id)
                data['prop'] =prop
                return render(request, template_name, data)
        except Coach.DoesNotExist:
            if  request.user.is_staff:
                data['teams'] = Team.objects.all().order_by('-pk')[:3]
                data['last_5_matchs'] = Match.objects.all().order_by('-pk')[:3]
                data['last_5_players'] = Player.objects.all().order_by('-pk')[:3]
                return render(request, template_name, data)
            else:
                data['teams'] = None
                data['last_5_matchs'] = None
                data['last_5_players'] = None
                return render(request, template_name, data)
    else:
        data['teams'] = None
        data['last_5_matchs'] = None
        data['last_5_players'] = None
        return render(request, template_name, data)



    return render(request, template_name, data)

@login_required
def indexCoach(request):
    template = 'acciones/list_coach.html'
    coachs = Coach.objects.all()
    context = {"Coach": coachs}
    return render(request, template, context)
@login_required
def Coach_create(request):
    template = 'acciones/form.html'
    form = CoachForm(request.POST,request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('Game:indexCoach')
    context = {"form": form}
    return render(request, template, context)

@login_required
def Coach_update(request, pk):
    template = 'acciones/form.html'
    Coachs = get_object_or_404(Coach, pk=pk)
    form = CoachForm(request.POST or None, instance=Coachs)
    if form.is_valid():
        form.save()
        return redirect('Game:indexCoach')
    context = {"form": form}
    return render(request, template, context)
@login_required
def Coach_delete(request, pk):
    template = 'acciones/delete.html'
    Coachs = get_object_or_404(Coach, pk=pk)
    if request.method == 'POST':
        Coachs.delete()
        return redirect('Game:indexCoach')
    context = {"Coach": Coach}
    return render(request, template, context)
@login_required
def indexTeam(request):
    
    template_name = 'acciones/Team_list.html'
    data = {}
    userrr = get_object_or_404(User, username=request.user.username)
    try:
        prop = Coach.objects.get(user=userrr)
        data['teams'] = Team.objects.filter(coach= request.user.coach.id)
        return render(request, template_name, data)
    except Coach.DoesNotExist:
        data['teams'] = Team.objects.all()
        return render(request, template_name, data)
    

@login_required
def Team_create(request):
    template = 'acciones/form.html'
    data = {}
    form = TeamForm(request.POST,request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('Game:indexTeam')
    context = {"form": form}
    return render(request, template, context)

@login_required
def Team_update(request, pk):
    template = 'acciones/form.html'
    teams = get_object_or_404(Team, pk=pk)
    form = TeamForm(request.POST or None, instance=teams)
    if form.is_valid():
        form.save()
        return redirect('Game:indexTeam')
    context = {"form": form}
    return render(request, template, context)
@login_required
def Team_delete(request, pk):
    template = 'acciones/delete.html'
    Teams = get_object_or_404(Team, pk=pk)
    if request.method == 'POST':
        Teams.delete()
        return redirect('Game:indexTeam')
    context = {"Team": Teams}
    return render(request, template, context)

@login_required
def indexPlayer(request):
    template = 'acciones/Player_list.html'
    Players = Player.objects.all()
    context = {"Player": Players}
    return render(request, template, context)

@login_required
def Player_create(request):
    template = 'acciones/form.html'
    form = PlayerForm(request.POST,request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('Game:indexPlayer')
    context = {"form": form}
    return render(request, template, context)

@login_required
def Player_update(request, pk):
    template = 'acciones/form.html'
    Players = get_object_or_404(Player, pk=pk)
    form = PlayerForm(request.POST or None, instance=Players)
    if form.is_valid():
        form.save()
        return redirect('Game:indexPlayer')
    context = {"form": form}
    return render(request, template, context)
@login_required
def Player_delete(request, pk):
    template = 'acciones/delete.html'
    Players = get_object_or_404(Player, pk=pk)
    if request.method == 'POST':
        Players.delete()
        return redirect('Game:indexPlayer')
    context = {"Player": Players}
    return render(request, template, context)
@login_required
def pperfil(request):
    if request.user.is_authenticated:
        template = 'acciones/miperfil.html'
        userrr = get_object_or_404(User, username=request.user.username)
        try:
            prop = Coach.objects.get(user=userrr)
            context = {"Userr": userrr,"Profile":prop}
            return render(request, template, context)
        except Coach.DoesNotExist:
            context = {"Userr": userrr}
            return render(request, template, context)
        

    else:    
        return render(request,'acciones/miperfil.html',)

def registro_view(request):
    
    if request.user.is_authenticated:
        return redirect('Game:home')

    form = registroForm (request.POST or None)

    if request.method == 'POST' and form.is_valid():       
        user = form.save()
        if user:
            messages.success(request, 'Usuario Creado Exitosamente')
            print("usuario Creado")
            return redirect('Game:home')
        else:
            print("usuario no creado")
            messages.error(request, 'Algunos de los campos no están correctos. Por favor, verifique los datos y vuelva a intentarlo.')


    return render(request,'acciones/registrar.html',{'form':form})

def login(request):
    
    form = AuthenticationForm()
    if request.method == "POST":
        
        form = AuthenticationForm(data=request.POST)
        
        if form.is_valid():
            
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

          
            user = authenticate(username=username, password=password)

            if user is not None:
               
                do_login(request, user)
             
                return redirect('Game:home')


    return render(request, "login.html", {'form': form})
@login_required
def logout(request):
    do_logout(request)

    return redirect('Game:home')
@login_required
def ChangePassword(request):
    print ("wenardo")

    usir = User.objects.get(username=request.user.username)
    print (usir.id)
    print(request.method)
    if request.method == 'POST':
        form = SetPasswordForm(usir, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  
            messages.success(request, 'Tu contraseña se actualizó correctamente!')
            return redirect('Usuarios:home')
        else:
            messages.error(request, 'Uuuy tenemos un error.')
    else:
        form = SetPasswordForm(usir)
    return render(request, 'acciones/cambio_pass.html', {
        'form': form
    })
@login_required
def editUser(request):

    form = actualiForm (instance = request.user)
    if request.method == 'POST':
        form = actualiForm(data=request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('Game:Perfil')
    
    return render(request,'acciones/editUser.html',{'form':form})
    
 


